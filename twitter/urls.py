from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^registro/', views.register, name='register'),
    url(r'^login/', views.login_user, name='login'),
]